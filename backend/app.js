const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(cors());

//DATABASE--------------------------------------------------------
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'web2_db'
})
connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected as ID: ' + connection.threadId);
});

//ACCOUNT--------------------------------------------------------
app.post('/login', function(req, res, next) {
    const username = req.body.username;
    const password = req.body.password;

    connection.query(
        "SELECT * FROM account WHERE username = ? AND password = ?", 
        [username, password], 
        (err, result) => {
            if(err){
                res.send({err: err});
            } 
            
            if (result.length > 0) {
                res.send(result);
            } else {
                res.send({message: "Wrong username/password!"});
            }
        }
    );
});

app.post('/register', function(req, res) {

    const username = req.body.username;
    const password = req.body.password;

    connection.query(
        "INSERT INTO account (username, password) VALUES (?,?)", 
        [username, password], 
        (err, result) => {
            console.log(err);
        }
    );
});

//-------------------------------------------------------------
function log(req, res, next) {
    console.log(req.method + " Request at" + req.url);
    next();
}
app.use(log);
    
var server = app.listen(port, function() {
    console.log("Listening to " + port);
});