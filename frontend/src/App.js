import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom'
import { LoginContext, UserContext} from './components/helper/Context';
import { useState } from 'react';

import data from './components/data';

import Header from './components/Header';
import Footer from './components/Footer';
import Home from './components/Home';
import Projects from './components/Projects';
import About from './components/About';
import Shop from './components/Shop';
import Login from './components/Login';
import Profile from './components/Profile';
import Cart from './components/Cart';
import Product from './components/Product';
import Checkout from './components/Checkout';
import Added from './components/Added';
import InProgress from './components/InProgress';

function App() {

  const [loggedIn, setLoggedIn] = useState(false);
  const [profileName, setProfileName] = useState("USER");
  const { items } = data;

  return (
    <Router>
      <div className="App">
        <Header />
        <Route path='/home' component={Home}/>
        <Route path='/projects' component={Projects}/>
        <Route path='/gallery' component={InProgress}/>
        <Route path='/about' component={About}/>
        <Route path='/shop' component={Shop}/>

        <LoginContext.Provider value={{ loggedIn, setLoggedIn }}>
        <UserContext.Provider value={{ profileName, setProfileName }}>
          <Route path='/login' component={Login}/>
          <Route path='/profile' component={Profile}/>
        </UserContext.Provider>
        </LoginContext.Provider>

        <Route path='/cart' render={(props) => (
            <Cart {...props} items={items} />
        )} />

        <Route path='/product' render={(props) => (
            <Product {...props} items={items} />
        )} />

        <Route path='/checkout' render={(props) => (
            <Checkout {...props} items={items} />
        )} />
        
        <Route path='/added' component={Added}/>
        <Route path='/inProgress' component={InProgress}/>
        <Footer />
        <Redirect to='/home' />
      </div>
    </Router>
  );
}

export default App;