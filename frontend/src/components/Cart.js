import { Link } from 'react-router-dom';

import './style/stylesheet.css';

const Checkout = (items) => {  //cant fix
    /*items.map((item) => ( 
        item.quanty = 0
    ))*/
}

const Cart = (props) => {
    let cost = 0;

    const { items } = props;

    items.map((item) => ( 
        cost+=(item.price*item.quanty)
    ))

    const remove = (item) => { //cant fix
        //item.quanty = 0;
    }

    return (
        <main className="main">
            <div id="cart1">
                <p id="cartHead">YOUR CART</p>
                    {items.map((item) => { 
                        if (item.quanty > 0) {
                            return (
                                <div className="cartItem">
                                    <p className="cartItemQuant">{item.quanty}x</p>
                                    <img className="cartItemImg" src={item.img} alt="pic"/>
                                    <p className="cartItemName">{item.name}</p>
                                    <p className="cartItemPrice">€{item.price*item.quanty}</p>
                                    <p className="cartItemRemove" onClick={remove(item)}>X</p>
                                </div>
                            )
                        } else {
                            return null;
                        }
                    }
                    )};
                <div id="cart1_1">
                </div>
                
                <p id="cartTotal">TOTAL: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; €{cost}</p>
                <Link id="cartContinue" to='/shop'>continue shopping</Link>
                <Link to='/checkout'>
                    <button id="cartButton" onClick={Checkout(items)} >CHECK OUT</button>
                </Link>
            </div>
            {items.map((item) => ( 
                <div className="cartFil">
                </div>
            ))}
        </main> 
    )
}
export default Cart