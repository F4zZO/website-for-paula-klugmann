
import { useLocation } from 'react-router';
import { useHistory } from 'react-router';

import './style/stylesheet.css';

const Product = (props) => {
    const location = useLocation();
    let history = useHistory();
    const { items } = props;
    let img;
    let name;
    let price;
    let text1;
    let text2;

    const addCart = () => {
        switch(location.pathname){
            case '/product/poster':
                items[0].quanty += 1;
                break;
                
            case '/product/sticker': 
                items[1].quanty += 1;
                break;
    
            case '/product/postcard': 
                items[2].quanty += 1;
                break;
    
            case '/product/headshot': 
                items[3].quanty += 1;
                break;
    
            case '/product/waistup': 
                items[4].quanty += 1;
                break;
    
            case '/product/emote': 
                items[5].quanty += 1;
                break;
    
            default:
                break;
        }
        history.push('/added');
    }

    switch(location.pathname){
        case '/product/poster':
            img = items[0].img;
            name = items[0].name;
            price = items[0].price;
            text1 = 'Physical print of the British Bricks illustration, designed by Paula.';
            text2 = 'Total size: A3 30cm x 42 cm (12" x 16 1/2"). Digitally printed on 100% cotton paper.';
            break;
        case '/product/sticker': 
            img = items[1].img;
            name = items[1].name;
            price = items[1].price;
            text1 = 'Physical printed stickers, designed by Paula.';
            text2 = '4 different Stickers in stylish colors.';
            break;
        case '/product/postcard': 
            img = items[2].img;
            name = items[2].name;
            price = items[2].price;
            text1 = 'Physical printed postcard, designed by Paula.';
            text2 = 'Total size: A5 148mm x 210m. Digitally printed on 100% cotton paper.';
            break;
        case '/product/headshot': 
            img = items[3].img;
            name = items[3].name;
            price = items[3].price;
            text1 = 'Physical headshot of the Character "Dana", designed by Paula.';
            text2 = 'Total size: A3 30cm x 42 cm (12" x 16 1/2"). Digitally printed on 100% cotton paper.';
            break;
        case '/product/waistup': 
            img = items[4].img;
            name = items[4].name;
            price = items[4].price;
            text1 = 'Physical waistupshot of the Character "Yuria", designed by Paula.';
            text2 = 'Total size: A3 30cm x 42 cm (12" x 16 1/2"). Digitally printed on 100% cotton paper.';
            break;
        case '/product/emote': 
            img = items[5].img;
            name = items[5].name;
            price = items[5].price;
            text1 = 'Emotes for your online (e.g. Twitch, Youtube, Discord,...) Chat, designed by Paula.';
            text2 = 'Total of 8 different Emotes just the way you want them.';
            break;
        default:
            img = '';
            name = '';
            price = 0;
            text1 = '';
            text2 = '';
            break;
    }

    return (
        <main className="main">
            <div id="product1">
              <div className="content">
                <div id="product1_1">
                    <img id="productPic" src={img} alt="pic"/>
                </div>
                <div id="product1_2">
                    <p id="productName">{name}</p>
                    <p id="productPrice">€{price}</p>
                    <button id="productButton" onClick={addCart}>Add to Cart</button>
                    <article id="productDesc">Description</article><br/><br/>
                    <article>{text1}</article><br/>
                    <article>{text2}</article>
                </div>            
              </div>
            </div>
        </main> 
    )
}
export default Product