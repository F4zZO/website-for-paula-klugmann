import Poster from '../images/shop/poster.png'
import Sticker from '../images/shop/stickers.png'
import Postcard from '../images/shop/postcard.png'
import Headshot from '../images/shop/headshot.png'
import Waistup from '../images/shop/waistup.png'
import Emotes from '../images/shop/emotes.png'

const data = {
    items: [
        {
            id: '0',
            img: Poster,
            name: 'Poster',
            price: 15,
            quanty: 0,
        },
        {
            id: '1',
            img: Sticker,
            name: 'Sticker',
            price: 5,
            quanty: 0,
        },
        {
            id: '2',
            img: Postcard,
            name: 'Postcard',
            price: 8,
            quanty: 0,
        },
        {
            id: '3',
            img: Headshot,
            name: 'Headshot',
            price: 20,
            quanty: 0,
        },
        {
            id: '4',
            img: Waistup,
            name: 'Waistup',
            price: 40,
            quanty: 0,
        },
        {
            id: '5',
            img: Emotes,
            name: 'Emotes',
            price: 45,
            quanty: 0,
        },
    ],
};
export default data;