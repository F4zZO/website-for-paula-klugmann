import './style/stylesheet.css';

import DokuBuch from '../images/decor/dokubuchwhite.jpg'
import ArtCat from '../images/art/ProjCats.png'
import Mockup from '../images/decor/mockup.jpg'

const Projects = () => {
    return (
        <main className="main">

        <div id="proj1">
          <div id="projFill"></div>
          <div className="content">
            <div id="proj1_1">
              <img src={DokuBuch} alt="decorBuch"/>
            </div>
            <div id="proj1_2">
              <div id="proj1_2_1">
                <p id="proj1Head">Design  Principles</p>
              
                <article>A design project made in Adobe</article>
                <article>inDesign with the goal</article>
                <article>to understand and apply the</article>
                <article>most important rules of design.</article><br/>
              </div>
              <a href="DesignPrinciples.html">
                <div className="divHov" id="proj1_2_2">
                  <b className="txt">see more</b><b className="arrow">-</b>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div id="proj2">
          <div className="content">
            <div id="proj2_1">
              <div id="proj2_1_1">
                <p id="proj2Head">Cat Soup</p>
              
                <article>A motion design project about a</article>
                <article>little cat that is taking a bath in a</article>
                <article>soup bowl, made in</article>
                <article>Adobe Animate.</article><br/>
              </div>
              <a href="Catsoup.html">
                <div className="divHov" id="proj2_1_2">
                  <b className="txt">see more</b><b className="arrow">-</b>
                </div>
              </a>
              
            </div>
            <div id="proj2_2">
              <img src={ArtCat} alt="artCat"/>
            </div>  
          </div>
        </div>
        <div id="proj3">
          <div className="content">
            <div id="proj3_1">
              <img src={Mockup} alt="decorMock"/>
            </div>
            <div id="proj3_2">
              <div id="proj3_2_1">
                <p id="proj3Head">The Positivity Project</p>
              
                <article>A webservice project about body</article>
                <article>positivity created in Adobe XD.</article><br/>
              </div>
              <a href="PositivityProj.html">
                <div className="divHov" id="proj3_2_2">
                  <b className="txt">see more</b><b className="arrow">-</b>
                </div>
              </a>
            </div>
          </div>
        </div>

        </main>  
    )
}
export default Projects