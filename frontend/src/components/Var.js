

import Poster from '../images/shop/poster.png'
import Sticker from '../images/shop/stickers.png'
import Postcard from '../images/shop/postcard.png'
import Headshot from '../images/shop/headshot.png'
import Waistup from '../images/shop/waistup.png'
import Emotes from '../images/shop/emotes.png'

const Items = [
    {
        img: Poster,
        name: 'Poster',
        price: 15,
        quant: 1,
    },
    {
        img: Sticker,
        name: 'Sticker',
        price: 5,
        quant: 1,
    },
    {
        img: Postcard,
        name: 'Postcard',
        price: 8,
        quant: 1,
    },
    {
        img: Headshot,
        name: 'Headshot',
        price: 20,
        quant: 1,
    },
    {
        img: Waistup,
        name: 'Waistup',
        price: 40,
        quant: 1,
    },
    {
        img: Emotes,
        name: 'Emotes',
        price: 45,
        quant: 1,
    }
];

export default Items