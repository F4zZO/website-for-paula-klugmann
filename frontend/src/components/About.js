import './style/stylesheet.css';

import Paula from '../images/decor/paulafoto.jpg'
import In from '../images/icon/id.png'
import Ps from '../images/icon/ps.png'
import Xd from '../images/icon/xd.png'
import Il from '../images/icon/ai.png'
import An from '../images/icon/an.png'
import Pr from '../images/icon/pr.png'
import Cs from '../images/icon/clip.png'
import Ma from '../images/icon/maya.png'
import Insta from '../images/icon/insta.png'
import Artstation from '../images/icon/artstation.png'
import Mail from '../images/icon/mail.png'
import Ashe from '../images/art/ashepng.png'

const About = () => {
    return (
        <main className="main">
        <div id="about1">
          <div className="content">
            <div id="about_photo">
              <img src={Paula} alt="Me"/>
            </div>
            <div id="about_BG">
              <div id="about_text">
                <p id="aboutHead">A little bit about myself</p>

                <article> My name is Paula Klugmann, I am 20 years old and am studying digital media</article> 
                <article> design in the 3rd semester in Germany. I started drawing and being creative </article>
                <article>about 7 years ago and tried to improve and broaden my skills ever since.</article>
                <article>Over the years I learned a lot about many different creative fields such as</article>
                <article>product design, mechanical drawing, UX/UI Design, website and magazine</article>
                <article>design as well as animation, traditional, digital and 3D art.</article>
                <article>My main focus however lays on stylized art as it is what brings me most joy.</article>
                <article>My goal is to work as a concept or character artist one day.</article> <br/>
                <article>PROGRAMS:</article>
              </div>
              <div id="about_icons">
                <img className ="icons" src={In} alt="Indesign"/>
                <img className ="icons" src={Ps} alt="Photoshop"/>
                <img className ="icons" src={Xd} alt="XD"/>
                <img className ="icons" src={Il} alt="Illustrator"/>
                <img className ="icons" src={An} alt="Animate"/>
                <img className ="icons" src={Pr} alt="Premiere"/>
                <img className ="icons" src={Cs} alt="Clip Studio Paint"/>
                <img className ="icons" src={Ma} alt="Maya"/>
              </div>
            </div>
          </div>
        </div>
        <div id="contact">
          <div className="content">
            <div id="contact_background">
              <div id="contact_text">
                <p id="aboutHead">Get in Touch</p>
                <br/>
                <article>If you stumble upon something that catches your eye on my </article> 
                <article>website, feel free to contact me via Email or social media!</article>
                <br/>
                <article>I am offering commissions for: Stylized portraits, character sheets</article>
                <article>and designs, Twitch overlays and emotes and similar things. </article>
              </div>
              <div id="contact_icons">
                <img className ="iconscontact" src={Mail} alt="Email"/>
                <a href={"https://www.instagram.com/pea_artworks/?hl=en" || "#" } target="_blank" rel="noreferrer">
                  <img className ="iconscontact" src={Insta} alt="Instagram"/>
                </a>
                <a href={"https://www.paulakl.artstation.com" || "#" } target="_blank" rel="noreferrer">
                  <img className ="iconscontact" src={Artstation} alt="Artstation"/>
                </a>
              </div>
              <div id="socials">
                <article >Paula.Klugmann@gmx.de</article><br/><br/><br/>
                <article>https://www.instagram.com/pea_artworks/</article><br/><br/><br/>
                <article>paulakl.artstation.com</article><br/><br/><br/>
              </div>
            </div>
            <div id="contact_photo">
              <img src={Ashe} alt="Art"/>
            </div>
          </div>
        </div>      
      </main>  
    )
}
export default About