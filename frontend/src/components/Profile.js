import React, { useContext } from 'react';
import { useHistory } from 'react-router';
import { LoginContext, UserContext } from './helper/Context';

import './style/stylesheet.css';

const Profile = () => {
    let head = "USER";
    let sign = "Logout";
    
    let history = useHistory();

    const {loggedIn, setLoggedIn} = useContext(LoginContext);
    const {profileName, setProfileName} = useContext(UserContext);

    if(!loggedIn){
        history.push('/login');
        setProfileName('USER')
    }

    head = profileName;

    const logout = () => {
        setLoggedIn(false);
    }

    return (
        <main className="main">
            <div id="profile1_1">
                <p id="profileHead">{head}</p>
                <button id="profileButton" onClick={logout}>{sign}</button>
            </div>
        </main> 
    )
}
export default Profile