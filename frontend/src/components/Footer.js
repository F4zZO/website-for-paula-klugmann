import { Link } from 'react-router-dom';
import './style/stylesheet.css';
import Insta from '../images/icon/insta.png'
import Artstation from '../images/icon/artstation.png'

const Footer = () => {
    return (
        <div id="footerDiv">
          <br/><br/>
          <p id="footerName">Paula Kl</p>
          <a href= {"https://www.instagram.com/pea_artworks/?hl=en" || "#"} target="_blank" rel="noreferrer">
            <img id="insta" src={Insta} alt="insta"/>
          </a>
          <a href={"https://www.paulakl.artstation.com" || "#" } target="_blank" rel="noreferrer">
            <img id="artstation" src={Artstation} alt="artstation"/>
          </a>
          <br/><br/>
          <Link id="footerCon" to="/about">contact</Link>
          <br/><br/>
          <article id="footerTxt">All images ©Paula Klugmann. Please do not use without permission</article>
          <br/>
        </div>
    )
}
export default Footer