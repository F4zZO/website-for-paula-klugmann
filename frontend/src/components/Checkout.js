import { Link } from 'react-router-dom';

import './style/stylesheet.css';

const Checkout = () => {
    return (
        <main className="main">
            <div id="checkout1">
                <p id="checkoutHead">THANK YOU FOR YOUR SUPPORT</p>
                <Link id="checkoutContinue" to='/shop'>back to shop</Link>
            </div>
        </main> 
    )
}
export default Checkout