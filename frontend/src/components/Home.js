import { Link } from 'react-router-dom';
import './style/stylesheet.css';

import HomeStripe from '../images/decor/HomeStripe.png'
import Itazura from '../images/art/HomeItazura.jpg'
import HomeCat from '../images/art/HomeCat.jpg'
import HomeGallery from '../images/art/HomeGallery.jpg'

const Home = (props) => {
    return (
        <main className ="main">
            
        <div id="home1">
          <div className="content">
            <div id="home1_1">
              <div id="home1_1_1">
                <span id="loading">In Progress</span>
              </div>
              <div id="home1_1_2">
              </div>
            </div>
            <div id="home1_2">
              <p id="home1Head">W e l c o m e</p>
              <article>My name is Paula Klugmann and this website</article>
              <article>serves as a protfolio of my projects and</article>
              <article>artworks up until now.</article><br/>
              <article>Take a look around!</article><br/>
            </div>
            <div id="home1_3">
              <img src={HomeStripe} alt="Stripe"/>
            </div>
          </div>
        </div>

        <div id="home2">
          <div className="content">
            <div id="home2_1">
              <div id="home2_1_1">
                <p id="home2Head">F e a t u r e d</p><br/>
              </div>
              <div id="home2_1_2">
                <div id="home2_1_2_1">
                  <article id="home2Txt">I T A Z U R A</article><br/>
                  <article>designing a 3D Character</article><br/>
                </div>
                <Link to="/inProgress">
                  <div className="divHov" id="home2_1_2_2">
                    <b className="txt" id="home2Txt2">see my progress</b><b className="arrow" id="home2Arrow">-</b>
                  </div>
                </Link>
              </div>
            </div>
            <div id="home2_2">
              <img src={Itazura} alt="Itazura"/>
            </div>
          </div>
        </div>

        <div id="home3">
          <div className="content">
            <div id="home3_1">
              <div id="home3_1_1">
                <img src={HomeCat} alt="Cat"/>
              </div>
              <div id="home3_1_2">
              </div>
            </div>
            <div id="home3_2">
              <div id="home3_2_1">
                <p id="home3Head">CAT SOUP - Latest Project</p>
                <article>check out my latest project</article>
                <article>CAT SOUP here!</article><br/>
              </div>
              <Link to="/inProgress">
                <div className="divHov" id="home3_2_2">
                  <b className="txt">see more</b><b className="arrow">-</b>
                </div>
              </Link>
            </div>
          </div>
        </div>

        <div id="home4">
          <div className="content">
            <div id="home4_1">
              <div id="home4_1_1">
                <p id="home4Head">G a l l e r y</p>
                <article>take a look at my art gallery</article>
              </div>
              <Link to="/inProgress">
                <div className="divHov" id="home4_1_2">
                  <b className="txt">discover</b><b className="arrow">-</b>
                </div>
              </Link>
            </div>
            <div id="home4_2">
              <div id="home4_2_1">
              <img src={HomeGallery} alt="Gallery"/>
              </div>
              <div id="home4_2_2">
              </div>
            </div>
          </div>
        </div>
            
        </main>
    )
}
export default Home