import { Link } from 'react-router-dom';
import './style/stylesheet.css';

import Poster from '../images/shop/poster.png'
import Sticker from '../images/shop/stickers.png'
import Postcard from '../images/shop/postcard.png'
import Headshot from '../images/shop/headshot.png'
import Waistup from '../images/shop/waistup.png'
import Emotes from '../images/shop/emotes.png'

const Shop = () => {

    return (
        <div className ="main">
            
            <div id="shop1">
              <div className="content">
                <div id="shop1_1">
                    <Link to="/product/poster">
                        <img id="poster" src={Poster} alt="poster"/>
                        <p id="itemName">POSTER</p>
                        <p id="itemPrice">€15</p>
                    </Link>
                </div>
                <div id="shop1_2">
                    <Link to="/product/sticker">
                        <img id="sticker" src={Sticker} alt="sticker"/>
                        <p id="itemName">STICKER</p>
                        <p id="itemPrice">€5</p>
                    </Link>
                </div>
                <div id="shop1_3">
                    <Link to="/product/postcard">
                        <img id="postcard" src={Postcard} alt="postcard"/>
                        <p id="itemName">POSTCARD</p>
                        <p id="itemPrice">€8</p>
                    </Link>
                </div>
              </div>
            </div>

            <div id="shop1">
              <div className="content">
                <div id="shop1_1">
                    <Link to="/product/headshot">
                        <img id="headshot" src={Headshot} alt="headshot"/>
                        <p id="itemName">HEADSHOT</p>
                        <p id="itemPrice">€20</p>
                    </Link>
                </div>
                <div id="shop1_2">
                    <Link to="/product/waistup">
                        <img id="waistup" src={Waistup} alt="waistup"/>
                        <p id="itemName">WAISTUP</p>
                        <p id="itemPrice">€40</p>
                    </Link>
                </div>
                <div id="shop1_3">
                    <Link to="/product/emote">
                        <img id="emotes" src={Emotes} alt="emotes"/>
                        <p id="itemName">EMOTE SET 1</p>
                        <p id="itemPrice">€45</p>
                    </Link>
                </div>
              </div>
            </div>


        </div>
    )
}
export default Shop