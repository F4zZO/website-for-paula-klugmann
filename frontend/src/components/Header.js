import { Link } from 'react-router-dom';
import { useLocation } from 'react-router';

import './style/stylesheet.css';
import Login from '../images/icon/login.png'
import Cart from '../images/icon/cart.png'

const Header = () => {
    const location = useLocation();
    return (
        <div>
            <div className="header">
                <div id="header1">
                    <Link to="/login">
                        <img id="login" src={Login} alt="login"/>
                    </Link>
                    <Link to="/cart">
                        <img id="cart" src={Cart} alt="cart"/>
                    </Link>
                </div>
                <div id="header2">
                    <p id="headerName">Paula Klugmann</p>
                </div>
            </div>
            
            <div className="nav">
                <li>                    
                    <Link id={(location.pathname==='/home') ? 'current' : null} to="/home">home</Link>
                    <b className="nav">|</b>
                    <Link id={(location.pathname==='/projects') ? 'current' : null} to="/projects">projects</Link>
                    <b className="nav">|</b>
                    <Link id={(location.pathname==='/gallery') ? 'current' : null} to="/gallery">gallery</Link>
                    <b className="nav">|</b>
                    <Link id={(location.pathname==='/about') ? 'current' : null} to="/about">about</Link>
                    <b className="nav">|</b>
                    <Link id={(location.pathname==='/shop' || location.pathname==='/cart' || location.pathname==='/product') ? 'current' : null} to="/shop">shop</Link>
                </li>
            </div>
        </div>
    )
}
export default Header