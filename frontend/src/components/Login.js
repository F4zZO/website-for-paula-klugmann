import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router';
import Axios from 'axios';

import { LoginContext, UserContext } from './helper/Context';

import './style/stylesheet.css';

const Login = () => {    

    const [username, setUser] = useState('');
    const [password, setPassword] = useState('');  

    const [loginStatus, setLoginStatus] = useState(0);

    const {loggedIn, setLoggedIn} = useContext(LoginContext);
    const {profileName, setProfileName} = useContext(UserContext);

    let history = useHistory();

    if(loggedIn){
        history.push('/profile');
    }

    const register = () => {
        Axios.post('http://localhost:5000/register', {
            username: username, 
            password: password, 
        }).then((response) => {
            console.log(response);
        })
    };

    const login = () => {
        Axios.post('http://localhost:5000/login', {
            username: username, 
            password: password, 
        }).then((response) => {
            if(response.data.message) {
                setLoginStatus(2)
            } else {
                setProfileName(username);
                console.log(profileName);
                setLoggedIn(true);
            }
        })
    };

    const swap = () => {
        if (loginStatus===1){
            setLoginStatus(0);
        } else {
            setLoginStatus(1);
        }
    };

    let head = "LOGIN";
    let errText = "";
    let create = "Create Account";
    let sign = "Sign in";
    let button = login;

    switch(loginStatus){
        case 0:
            head = "LOGIN";
            errText = "";
            create = "Create Account";
            sign = "Sign in";
            button = login;
            break;

        case 1:
            head = "REGISTER";
            button = register;
            sign = "Create Account";
            create = "already have an Account?";
            break;

        case 2:
            errText = "Wrong username/password"
            break;

        default:
            break;
    }

    return (
        <main className="main">
            <div id="login1_1">
                <p id="loginHead">{head}</p>
                <p id="loginErr">{errText}</p>
                <div>
                    <p id="loginMail">Email</p>
                    <input className="loginInput" onChange={ (e)=> {setUser(e.target.value);} }/>
                    <p id="loginPw">Password</p>
                    <input className="loginInput" onChange={ (e)=> {setPassword(e.target.value);} }/>
                </div>
                <button id="loginButton" onClick={button}>{sign}</button>
                <p id="loginCreate" onClick={swap} >{create}</p>
            </div>
        </main> 
    )
}
export default Login