import React from 'react';
import { useHistory } from 'react-router';

import './style/stylesheet.css';

const Added = () => {
    const history = useHistory();
    const back = () => {
        history.push('/shop');
    }
    const tocart = () => {
        history.push('/cart');
    }

    return (
        <main className="main">
            <div id="Add1_1">
                <p id="AddHead">Item was added</p>
                <button id="AddButton" onClick={back}>back to store</button>
                <button id="AddCartButton" onClick={tocart}>continue to cart</button>
            </div>
        </main> 
    )
}
export default Added